hangmansolver
=============

Beats hangman given a good dictionary, or a matching dictionary to the hangman game.

How to use:

For any letter you don't know use -

So, lets assume the word you were decided to have it solve was 'boat'

Starting out, you don't 'know' any letter, so when it asks, "Hangman Word:" input: ----
For "Attempted letters:" leave blank at this point, because we haven't attempted any letters yet. (hit enter)
It will output:
Guess: 'e'
Number of possible words: 1125 

(hit enter)

Ok, do this all again, as we still don't 'know' any words, use ----, but this time for Attempted letters type e then hit enter, as we attempted it, but it failed.  After it will tell you to guess 'a'.  Since this is correct you should use the following as your "hangman word:"

--a- ( Which translates to [ unknown, unknown, a, unknown ]

Best of luck with your solving
